from django.shortcuts import render
from django.http import HttpResponse
from rest_framework import generics, permissions, status
from blog.models import *
from blog.serializers import *
from rest_framework.response import Response

# Create your views here.

def index(request):
    return HttpResponse("Bienvenue dans le boutique en ligne.")

    #Ajout, modification, suppression pour le modele Client

class ClientListCreateView(generics.CreateAPIView):
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = ClientSerializer
    def get(self, request, *args, **kwargs):

        client = Client.objects.all()

        if not client:
            return Response({
                "status": "failure",
                "message": "no such item.",

            }, status=status.HTTP_404_NOT_FOUND)
        serializer= ClientSerializer(client, many=True)

        return Response({
            "status": "success",
            "message": "items successfully retrieved.",
            "count": client.count(),
            "data": serializer.data

            }, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):

        serializer= ClientSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": serializer.errors,
                "error": "erreur"

            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
                "status": "success",
                "message": "items successfully created",

            }, status=status.HTTP_201_CREATED)

class ClientUpdateView(generics.CreateAPIView):
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = ClientSerializer
    def put(self, request, *args, **kwargs):
        try:
            client = Client.objects.get(id=kwargs['pk'])
        except Client.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)
        client_data = {
            'nom': request.data['nom'],
            'prenom': request.data['prenom'],
            'adresse': request.data['adresse'],
            'telephone': request.data['telephone'],
            'photo': request.data['photo'],

        }

        serializer = ClientSerializer(article, data=client_data, partial=True)

        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": "invalid data",
                "error": serializer.errors

            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
                "status": "success",
                "message": "items successfully update",
                "data": serializer.data

        }, status=status.HTTP_200_OK)


class ClientDeleteView(generics.CreateAPIView):
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = CommandeSerializer
    def post(self, request, *args, **kwargs):
        try:
            client = Client.objects.get(id=kwargs['pk'])
        except Client.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)
        client.delete()
        return Response({
                "status": "success",
                "message": "items successfully deleted",

        }, status=status.HTTP_200_OK)


        #Ajout, modification, suppression pour le modele Lunette
class LunetteListCreateView(generics.CreateAPIView):
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = LunetteSerializer
    def get(self, request, *args, **kwargs):

        lunette = Lunette.objects.all()

        if not lunette:
            return Response({
                "status": "failure",
                "message": "no such item.",

            }, status=status.HTTP_404_NOT_FOUND)
        serializer= LunetteSerializer(lunette, many=True)

        return Response({
            "status": "success",
            "message": "items successfully retrieved.",
            "count": lunette.count(),
            "data": serializer.data

            }, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):

        serializer= LunetteSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": serializer.errors,
                "error": "erreur"

            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
                "status": "success",
                "message": "items successfully created",

            }, status=status.HTTP_201_CREATED)

class LunetteUpdateView(generics.CreateAPIView):
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = LunetteSerializer
    def put(self, request, *args, **kwargs):
        try:
            lunette = Lunette.objects.get(id=kwargs['pk'])
        except Lunette.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)
        lunette_data = {
            'nom': request.data['nom'],
            'typpe': request.data['typpe'],
            'prix': request.data['prix'],
            'photos': request.data['photos'],
            

        }

        serializer = LunetteSerializer(lunette, data=lunette_data, partial=True)

        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": "invalid data",
                "error": serializer.errors

            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
                "status": "success",
                "message": "items successfully update",
                "data": serializer.data

        }, status=status.HTTP_200_OK)


class LunetteDeleteView(generics.CreateAPIView):
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = LunetteSerializer
    def post(self, request, *args, **kwargs):
        try:
            lunette = Lunette.objects.get(id=kwargs['pk'])
        except Lunette.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)
        lunette.delete()
        return Response({
                "status": "success",
                "message": "items successfully deleted",

        }, status=status.HTTP_200_OK)


        #Ajout, modification, suppression pour le modele Commande

class CommandeListCreateView(generics.CreateAPIView):
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = CommandeSerializer
    def get(self, request, *args, **kwargs):

        commande = Commande.objects.all()

        if not commande:
            return Response({
                "status": "failure",
                "message": "no such item.",

            }, status=status.HTTP_404_NOT_FOUND)
        serializer= CommandeSerializer(commande, many=True)

        return Response({
            "status": "success",
            "message": "items successfully retrieved.",
            "count": commande.count(),
            "data": serializer.data

            }, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):

        serializer= CommandeSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": serializer.errors,
                "error": "erreur"

            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
                "status": "success",
                "message": "items successfully created",

            }, status=status.HTTP_201_CREATED)

class CommandeUpdateView(generics.CreateAPIView):
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = CommandeSerializer
    def put(self, request, *args, **kwargs):
        try:
            commande = Commande.objects.get(id=kwargs['pk'])
        except Commande.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)
        commande_data = {
            'client_id': request.data['client_id'],
            'lunette_id': request.data['lunette_id'],
            'date': request.data['date'],
            'nbre_lunettes': request.data['nbre_lunettes'],
            'montant_total': request.data['montant_total'],

        }

        serializer = CommandeSerializer(commande, data=commande_data, partial=True)

        if not serializer.is_valid():
            return Response({
                "status": "failure",
                "message": "invalid data",
                "error": serializer.errors

            }, status=status.HTTP_400_BAD_REQUEST)

        serializer.save()
        return Response({
                "status": "success",
                "message": "items successfully update",
                "data": serializer.data

        }, status=status.HTTP_200_OK)

class CommandeDeleteView(generics.CreateAPIView):
    permission_classes = (
        permissions.IsAuthenticated,
    )
    serializer_class = CommandeSerializer
    def post(self, request, *args, **kwargs):
        try:
            commande = Commande.objects.get(id=kwargs['pk'])
        except Commande.DoesNotExist:
            return Response({
                "status": "failure",
                "message": "no such item"
            }, status=status.HTTP_400_BAD_REQUEST)
        commande.delete()
        return Response({
                "status": "success",
                "message": "items successfully deleted",

        }, status=status.HTTP_200_OK)



class ClientByCommande(generics.CreateAPIView):
    permission_classes = (
        permissions.IsAuthenticated,
    )
    queryset=Client.objects.all()
    serializer_class=ClientSerializer

    def get(self, request, *args, **kwargs):

        client = Client.objects.filter(commande=kwargs['pk'])

        if not client:
            return Response({
                "status": "failure",
                "message": "no such item.",

            }, status=status.HTTP_404_NOT_FOUND)
        serializer= ClientSerializer(client, many=True)

        return Response({
            "status": "success",
            "message": "items successfully retrieved.",
            "count": client.count(),
            "data": serializer.data

            }, status=status.HTTP_200_OK)

