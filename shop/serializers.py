from rest_framework import serializers
from shop.models import Client, Lunette, Commande

class ClientSerializer(serializers.ModelSerializer):
	commande = CommandeSerializer()
    class Meta:
        model = Client
        fields='__all__'

class LunetteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lunette
        fields='__all__'



class CommandeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Commande
        fields='__all__'

