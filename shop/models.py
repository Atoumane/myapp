from django.db import models
import datetime


# Create your models here.
class Client (models.Model):
    nom = models.CharField(max_length=100, blank=False, null=False)
    prenom = models.CharField(max_length=1000, blank=False, null=False)
    adresse= models.CharField(max_length=1000, blank=True, null=True)
    telephone= models.IntegerField(blank=True, null=True)
    photo= models.ImageField(upload_to = 'pictures/items/', null=True)
    commande = models.ForeignKey('Commande', on_delete=models.CASCADE, null=True, blank=True)

    


    def __str__(self):
       return 'Nom : '+str(self.nom)+'\n prenom:  '+str(self.prenom)+'\n adresse:  '+str(self.adresse)+'\n telephone:  '+str(self.telephone)+'\n photo:  '+str(self.photo)

    class Meta:
    	db_table = 'myapp_client'
    	ordering = ['nom']
     



class Lunette (models.Model):
    nom = models.CharField(max_length=100, blank=False, null=False)
    typpe = models.CharField(max_length=1000, blank=False, null=False)
    prix= models.IntegerField(blank=True, null=True)
    photos = models.ImageField(upload_to = 'pictures/items/', null=True)

    def __str__(self):
       return 'Nom : '+str(self.nom)+'\n typpe:  '+str(self.typpe)+'\n prix:  '+str(self.prix)+'\n photos: '+str(self.photos)

    class Meta:
    	db_table = 'myapp_lunette'
    	ordering = ['nom']
     


class Commande (models.Model):
    client_id = models.AutoField(primary_key=True)
    lunette_id = models.IntegerField(blank=False, null=False)
    date= models.DateField(null=False, default=datetime.datetime.now)
    nbre_lunettes = models.IntegerField(blank=False, null=False)
    montant_total = models.IntegerField(blank=False, null=False)
    


    def __str__(self):
       return 'client_id : '+str(self.client_id)+'\n lunette_id:  '+str(self.lunette_id)+'\n date: '+str(self.date)+'\n nbre_lunettes:  '+str(self.nbre_lunettes)+'\n montant_total: '+str(self.montant_total)

    class Meta:
    	db_table = 'myapp_commande'
    	ordering = ['client_id']
     