#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      Yaye6c
#
# Created:     30/04/2019
# Copyright:   (c) Yaye6c 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------

from django.urls import path
from django.conf.urls import url


from . import views
from rest_framework import routers

router =routers.DefaultRouter()

urlpatterns = [
    path('',views.index, name='index'),

    #URLS pour la classe Client
    
    url(r'^client/$',views.ClientListCreateView.as_view(), name='client'),
    url(r'^clientupdate/(?P<pk>[0-9]+)/$',views.ClientUpdateView.as_view()),
    url(r'^clientdelete/(?P<pk>[0-9]+)/$',views.ClientDeleteView.as_view()),
    url(r'^clientcreate/(?P<pk>[0-9]+)/$',views.ClientCreateView.as_view()),

    #URLS pour la classe Lunette
    
    url(r'^lunette/$',views.LunetteListCreateView.as_view(), name='lunette'),
    url(r'^lunetteupdate/(?P<pk>[0-9]+)/$',views.LunetteUpdateView.as_view()),
    url(r'^lunettedelete/(?P<pk>[0-9]+)/$',views.LunetteDeleteView.as_view()),
    url(r'^lunetteCreate/(?P<pk>[0-9]+)/$',views.LunetteCreateView.as_view()),


    #URLS pour la classe Commande
    
    url(r'^commande/$',views.CommandeListCreateView.as_view(), name='commande'),
    url(r'^commandeupdate/(?P<pk>[0-9]+)/$',views.CommandeUpdateView.as_view()),
    url(r'^commandedelete/(?P<pk>[0-9]+)/$',views.CommandeDeleteView.as_view()),
    url(r'^commandeCreate/(?P<pk>[0-9]+)/$',views.CommandeCreateView.as_view()),
    
    url(r'^clientByCommande/(?P<pk>[0-9]+)/$',views.ClientByCommande.as_view()),

    url(r'^login/$', obtain_jwt_token),
]