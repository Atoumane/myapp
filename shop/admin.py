from django.contrib import admin
from shop.models import Client, Commande, Lunette

# Register your models here.
admin.site.register(Client)
admin.site.register(Commande)
admin.site.register(Lunette)

